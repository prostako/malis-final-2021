from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import random
from toolkit.loaders.loader_eth import load_eth
import copy
from sklearn.metrics import r2_score
import math
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from scipy.stats import gaussian_kde
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVR
from sklearn.preprocessing import MinMaxScaler

# Library is the most important python file that stores all our customized functions.

# Used to open the two datasets


def load_datasets():
    eth = load_eth('./ETH/seq_eth/obsmat.txt')
    eth_hotel = load_eth('./ETH/seq_hotel/obsmat.txt')

    trajs = eth.get_trajectories()
    trajs_hotel = eth_hotel.get_trajectories()
    return trajs, trajs_hotel


# Get data_data  together with get_specified_data and get_x_y_train_test (or get_x_y_train_test_velocity) turn the unusable dataframe into usable matrices for
# the machine models. They only use persons that appeared on the video for a specified amount of frames.
# They returns X and y for the machine learning model, they can support multi_output and velocity.
def get_data(my_trajs, person_range=None):

    person_array = []

    for key in my_trajs.groups.keys():
        if person_range is None or key[1] in person_range:

            person_df = my_trajs.get_group(key)

            person = person_df[["pos_x", "pos_y",
                                "vel_x", "vel_y", "timestamp"]]
            person.index = np.arange(0, len(person))
            person_array.append(person)
    return person_array


def get_specified_data(my_trajs, train_range, test_pos, person_range=None, test_range=False):

    try:

        if train_range.start < 0:
            raise ValueError("train_range.stop has to be at least 0")
        if train_range.stop < train_range.start+1:
            raise ValueError(
                "train_range.stop has to be at least 1 higher than train_range.start")

        if train_range.stop > test_pos:
            raise ValueError("train_range.stop cannot be larger than pred_pos")
        person_array = get_data(my_trajs, person_range)
        if test_range:
            my_train_array = [
                person for person in person_array if test_pos-1 < person.shape[0]]
            my_test_array = [
                person for person in person_array if test_pos-1 < person.shape[0]]
        else:
            my_train_array = [
                person for person in person_array if test_pos < person.shape[0]]
            my_test_array = [
                person for person in person_array if test_pos < person.shape[0]]

        pred_range = range(train_range.stop, test_pos)

        for current_index, person in enumerate(my_train_array):

            train_person = copy.deepcopy(person)
            test_person = copy.deepcopy(person)

            for index, row in person.iterrows():

                if index not in train_range:
                    train_person.drop(index, inplace=True)

                # make predictions for the next range of pos, not 1
                if test_range:
                    if index not in pred_range:
                        test_person.drop(index, inplace=True)
                else:
                    if index != test_pos:
                        test_person.drop(index, inplace=True)

            train_person.index = np.arange(0, len(train_person))
            test_person.index = np.arange(0, len(test_person))

            my_train_array[current_index] = train_person
            my_test_array[current_index] = test_person

        return my_train_array, my_test_array

    except ValueError as err:
        print(err)


def get_x_y_train_test(known_array, pred_array, x_or_y_coor):

    X = known_array[0]['pos_'+x_or_y_coor].to_numpy()
    y = pred_array[0]['pos_'+x_or_y_coor].to_numpy()

    for current_index, person in enumerate(known_array):
        if current_index > 0:
            X = np.vstack(
                (X, known_array[current_index]['pos_'+x_or_y_coor].to_numpy()))
            #y = np.vstack((y,pred_array[current_index]['pos_'+x_or_y_coor].to_numpy()))

    for current_index, person in enumerate(pred_array):
        if current_index > 0:
            #X = np.vstack((X,known_array[current_index]['pos_'+x_or_y_coor].to_numpy()))
            y = np.vstack(
                (y, pred_array[current_index]['pos_'+x_or_y_coor].to_numpy()))

    return X, y


def get_x_y_train_test_velocity(known_array, pred_array, x_or_y_coor, multi_output=False):

    known_ts = known_array[0]["timestamp"].to_numpy()
    pred_ts = pred_array[0]["timestamp"].to_numpy()
    if multi_output:
        pred_ts = pred_ts[0].reshape(1)

    for row_nr in range(1, known_ts.shape[0]):
        pred_ts_add = pred_array[0]["timestamp"].to_numpy()
        if multi_output:
            pred_ts_add = pred_ts_add[0].reshape(1)
        pred_ts = np.hstack((pred_ts, pred_ts_add))

    X = known_array[0]['pos_'+x_or_y_coor].to_numpy() + \
        known_array[0]['vel_'+x_or_y_coor].to_numpy()*(pred_ts-known_ts)

    for current_index, person in enumerate(known_array):
        if current_index > 0:
            known_ts = known_array[0]["timestamp"].to_numpy()
            pred_ts = pred_array[0]["timestamp"].to_numpy()
            if multi_output:
                pred_ts = pred_ts[0].reshape(1)

            for row_nr in range(1, known_ts.shape[0]):
                pred_ts_add = pred_array[0]["timestamp"].to_numpy()
                if multi_output:
                    pred_ts_add = pred_ts_add[-1].reshape(1)
                pred_ts = np.hstack((pred_ts, pred_ts_add))

            X = np.vstack((X, known_array[current_index]['pos_'+x_or_y_coor].to_numpy(
            )+known_array[current_index]['vel_'+x_or_y_coor].to_numpy()*(pred_ts-known_ts)))

    return X

# Manages get_specified_data and get_x_y_train_test. Especially important if we want to split the data for x and y coordinates.


def eth_data_manager(trajs, train_range, test_pos, train_vel=False, multi_output=False, get_y=True):
    try:
        if multi_output:
            known_array, pred_array = get_specified_data(
                trajs, train_range, test_pos, test_range=True)
        else:
            known_array, pred_array = get_specified_data(
                trajs, train_range, test_pos)

        X_x_coor, y_x_coor = get_x_y_train_test(known_array, pred_array, "x")
        X_y_coor, y_y_coor = get_x_y_train_test(known_array, pred_array, "y")

        if train_vel:

            X_x_coor_vel = get_x_y_train_test_velocity(
                known_array, pred_array, "x", multi_output=multi_output)
            X_y_coor_vel = get_x_y_train_test_velocity(
                known_array, pred_array, "y", multi_output=multi_output)

            X_x_coor = np.hstack((X_x_coor, X_x_coor_vel))
            X_y_coor = np.hstack((X_y_coor, X_y_coor_vel))

        if multi_output:
            X = np.hstack((X_x_coor, X_y_coor))
            if get_y == False:
                return X
            y = np.hstack((y_x_coor, y_y_coor))

            return X, y

        else:
            if get_y == False:
                return X_x_coor, X_y_coor
            return X_x_coor, y_x_coor.ravel(), X_y_coor, y_y_coor.ravel()
    except ValueError as err:
        print(err)

# Manages the gridsearch for model. Returns the best results and the best model


def eth_model_manager(model, X_x_coor, y_x_coor, X_y_coor, y_y_coor, return_model=False):
    GS_x_coor = copy.deepcopy(model)
    GS_y_coor = copy.deepcopy(model)

    GS_x_coor.fit(X_x_coor, y_x_coor)
    GS_y_coor.fit(X_y_coor, y_y_coor)

    best_model_x_coor = GS_x_coor.best_estimator_
    best_model_y_coor = GS_y_coor.best_estimator_

    y_hut_x_coor = best_model_x_coor.predict(X_x_coor)
    y_hut_y_coor = best_model_y_coor.predict(X_y_coor)
    if return_model:
        return y_hut_x_coor, y_hut_y_coor, best_model_x_coor, best_model_y_coor
    else:
        return y_hut_x_coor, y_hut_y_coor

# Prints results in notebook output of the eth_model_manager


def print_results(y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, X_x_coor, X_y_coor, search_space=None, best_model_x_coor=None, best_model_y_coor=None, description="No description"):
    print("\n", description)

    print("Deviation Total/Per Person/Percentage (Deviation:Total Distance Walked)")
    print(coor_deviation(y_hut_x_coor, y_x_coor,
          y_hut_y_coor, y_y_coor, X_x_coor, X_y_coor))

    print("Box Surface/X-95-Quantile/Y-95-Quantile")
    print(coor_bounding_box_95(y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor))
    if search_space is None or best_model_x_coor is None or best_model_y_coor is None:
        return
    all_param_x_coor = best_model_x_coor.get_params()
    all_param_x_coor_print = {}
    all_param_y_coor = best_model_y_coor.get_params()
    all_param_y_coor_print = {}
    for key in search_space:
        all_param_x_coor_print[key] = all_param_x_coor[key]
    print("Params x_coor: ")
    print(all_param_x_coor_print)

    for key in search_space:
        all_param_y_coor_print[key] = all_param_y_coor[key]
    print("Params y_coor: ")
    print(all_param_y_coor_print)

# Used to calculate ADE (average of all rows of compare vector), FDE (last row of compare vector)


def coor_deviation(x_coor_pred, x_coor_actual, y_coor_pred, y_coor_actual, X_x_coor=None, X_y_coor=None):
    result_vector = ((((x_coor_pred - x_coor_actual)**2) +
                     ((y_coor_pred-y_coor_actual)**2))**0.5)
    if X_x_coor is not None and X_y_coor is not None:
        x_coor_compare = X_x_coor[:, -1]
        y_coor_compare = X_y_coor[:, -1]
        compare_vector = ((((x_coor_compare - x_coor_actual)**2) +
                          ((y_coor_compare-y_coor_actual)**2))**0.5)
        return np.sum(result_vector), np.average(result_vector), np.sum(result_vector)/np.sum(compare_vector)
    else:
        return np.sum(result_vector), np.average(result_vector)

# Calculates box that contains the right point to 90%


def coor_bounding_box_95(x_coor_pred, x_coor_actual, y_coor_pred, y_coor_actual):

    result_vector_x = abs(x_coor_pred - x_coor_actual)
    result_vector_y = abs(y_coor_pred - y_coor_actual)
    x_quantile = np.quantile(result_vector_x, 0.95)
    y_quantile = np.quantile(result_vector_y, 0.95)
    surface = 4*x_quantile*y_quantile

    return surface, x_quantile, y_quantile

# Add result to df


def add_to_result_df(df, train_range_pos, pred_pos, sample_len, y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, X_x_coor, X_y_coor, description=None):
    dev_total, dev_per_pers, dev_percent = coor_deviation(
        y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, X_x_coor, X_y_coor)
    box_sur, x_quant, y_quant = coor_bounding_box_95(
        y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor)
    dict = {"Description": description, "Train Pos": train_range_pos, "Pred Pos": pred_pos, "Sample Length": sample_len, "Dev Total": dev_total,
            "Dev per Person": dev_per_pers, "Dev Percentage": dev_percent, "Box Surface": box_sur, "X-95": x_quant, "Y-95": y_quant}
    df = df.append(dict, ignore_index=True)
    return df

# Alternative to add_to_result_df


def add_to_result_df_no_percentage(df, train_range_pos, pred_pos, sample_len, y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, description=None):
    dev_total, dev_per_pers = coor_deviation(
        y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor)
    box_sur, x_quant, y_quant = coor_bounding_box_95(
        y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor)
    dict = {"Description": description, "Train Pos": train_range_pos, "Pred Pos": pred_pos, "Sample Length": sample_len,
            "Dev Total": dev_total, "Dev per Person": dev_per_pers, "Box Surface": box_sur, "X-95": x_quant, "Y-95": y_quant}
    df = df.append(dict, ignore_index=True)
    return df

# Use dataframe to print the size of the 90% confidence boxes


def print_boxes(my_df, headline, row, save_path=None):
    try:
        col = math.ceil(my_df.shape[0]/row)
        if row < 2 or col < 2:
            raise ValueError("nr of rows and cols has to be at least 2")
        col = math.ceil(my_df.shape[0]/row)

        fig, axes = plt.subplots(row, col)
        fig.set_figheight(15)
        fig.set_figwidth(15)
        fig.tight_layout(pad=3.0, rect=[0, 0.03, 1, 0.95])
        fig.suptitle(headline, fontsize=16)
        for row_index, axe_row in enumerate(axes):

            for col_index, axe in enumerate(axe_row):

                current_index = len(axe_row)*row_index+col_index
                if current_index >= my_df.shape[0]:
                    break
                rect = matplotlib.patches.Rectangle((0, 0),
                                                    my_df["X-95"][current_index], my_df["Y-95"][current_index],
                                                    color='green')
                axe.set_aspect('equal', 'box')
                axe.set(xlim=(0, 3), ylim=(0, 3))
                axe.add_patch(rect)
                axe.set_xlabel("Box Surface: "+str("%.6f" %
                               my_df["Box Surface"][current_index]))
                calc_second = (current_index+1)*0.4
                axe.set_title("Second: "+str("%.2f" % calc_second) +
                              " "+my_df["Description"][current_index])

                axe.plot()
                if save_path is not None:
                    plt.savefig(save_path+headline+".pdf")
    except ValueError as err:
        print(err)

# Print the trajctory including the confidence box


def print_trajectory(y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, X_x_coor, X_y_coor, nr_trajs, row, headline):

    try:

        col = math.ceil(nr_trajs/row)
        if row < 2 or col < 2:
            raise ValueError("nr of rows and cols has to be at least 2")

        surface, box_x, box_y = coor_bounding_box_95(
            y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor)

        fig, axes = plt.subplots(row, col)
        fig.set_figheight(15)
        fig.set_figwidth(15)
        #fig.tight_layout(pad=3.0,rect=[0, 0.03, 1, 0.95])
        fig.suptitle(headline, fontsize=16)

        y_x_coor = y_x_coor[:nr_trajs]
        y_y_coor = y_y_coor[:nr_trajs]

        y_hut_x_coor = y_hut_x_coor[:nr_trajs]
        y_hut_y_coor = y_hut_y_coor[:nr_trajs]

        X_x_coor = X_x_coor[:nr_trajs, :]
        X_y_coor = X_y_coor[:nr_trajs, :]

        for row_index, axe_row in enumerate(axes):

            for col_index, axe in enumerate(axe_row):

                current_index = len(axe_row)*row_index+col_index
                if current_index >= nr_trajs:
                    break

                rect = matplotlib.patches.Rectangle((y_hut_x_coor[current_index]-box_x, y_hut_y_coor[current_index]-box_y),
                                                    2*box_x, 2*box_y, fc=(0, 1, 0, 0.5))
                axe.add_patch(rect)

                axe.set_aspect('equal', 'box')
                axe.set(ylim=(-2.5, 10), xlim=(-2, 12))
                axe.scatter(X_x_coor[current_index],
                            X_y_coor[current_index], color="blue")
                axe.scatter(y_x_coor[current_index],
                            y_y_coor[current_index], color="red")
                axe.scatter(y_hut_x_coor[current_index],
                            y_hut_y_coor[current_index], color="green")
                axe.set_xlabel("Box Surface: "+str("%.6f" % surface))
                axe.set_title("Example ")
    except ValueError as err:
        print(err)

# adapted from https://github.com/umautobots/bidireaction-trajectory-prediction/blob/296a50126cd50a1d4a0395696a0567575c4d4df8/bitrap/engine/evaluate.py#L122


def compute_kde_nll(pred_traj, target_traj):
    '''
    pred_traj: (T, 2)
    '''
    kde_ll = 0.

    log_pdf_lower_bound = -20
    # TODO reshape to (N, 2)
    T, _ = pred_traj.shape
    kde_ll_per_step = np.zeros(T)
    for timestep in range(T):
        try:
            kde = gaussian_kde(pred_traj[timestep, :].T)
            pdf = np.clip(kde.logpdf(
                target_traj[timestep].T), a_min=log_pdf_lower_bound, a_max=None)[0]
            kde_ll += pdf / T
            kde_ll_per_step[timestep] += pdf
        except np.linalg.LinAlgError:
            kde_ll = np.nan
            kde_ll_per_step[timestep] = np.nan
    return -kde_ll, -kde_ll_per_step

# Get all the best 24 models. Some can be Ridge/LR some can be SVR


def get_best_model_list(trajs, train_range_pos, end_pred_pos, print_model=False, mode="LR", print_traj=False, save_path=None):
    my_error = "neg_root_mean_squared_error"
    my_cv = 4
    my_n_jobs = -1

    best_model_x_coor_list = []
    best_model_y_coor_list = []

    column_names = ["Description", "Train Pos", "Pred Pos", "Sample Length",
                    "Dev Total", "Dev per Person", "Box Surface", "X-95", "Y-95"]
    result_df = pd.DataFrame(columns=column_names)

    for current_pred_pos in range(train_range_pos, end_pred_pos):
        train_range = range(train_range_pos)

        X_x_coor, y_x_coor, X_y_coor, y_y_coor, = eth_data_manager(
            trajs, train_range, current_pred_pos, train_vel=True, get_y=True)
        if mode == "LR":
            search_space_comp = {}
            estimator_comp = LinearRegression()
        elif mode == "Ridge":
            search_space_comp = {}
            estimator_comp = Ridge()
        GS_comp = GridSearchCV(estimator=estimator_comp,
                               param_grid=search_space_comp,
                               scoring=[my_error],
                               refit=my_error,
                               cv=my_cv,
                               verbose=0,
                               n_jobs=my_n_jobs)

        y_hut_x_coor_comp, y_hut_y_coor_comp, best_model_x_coor_comp, best_model_y_coor_comp = eth_model_manager(
            GS_comp, X_x_coor, y_x_coor, X_y_coor, y_y_coor, True)
        dev_total_comp, dev_per_person_comp = coor_deviation(
            y_hut_x_coor_comp, y_x_coor, y_hut_y_coor_comp, y_y_coor)

        description = "SVR scale "+str(current_pred_pos)+" "
        # dictionary for search space
        estimator = Pipeline([('scl', MinMaxScaler()), ('reg', SVR())])
        # dictionary for search space

        search_space = {
            "reg__kernel": ["linear", "rbf"],
            "reg__C": [2.0, 5.0, 10.0, 25.0, 50.0, 80.0, 100.0, 300.0, 500.0],
            "reg__epsilon": [0.0001, 0.0002, 0.0003, 0.0005, 0.001, 0.002, 0.005, 0.01]
        }
        # make a GridSerachCV object
        GS = GridSearchCV(estimator=estimator,
                          param_grid=search_space,
                          scoring=[my_error],
                          refit=my_error,
                          cv=my_cv,
                          verbose=0,
                          n_jobs=my_n_jobs)

        y_hut_x_coor, y_hut_y_coor, best_model_x_coor, best_model_y_coor = eth_model_manager(
            GS, X_x_coor, y_x_coor, X_y_coor, y_y_coor, True)
        if print_model:
            print_results(y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, X_x_coor, X_y_coor,
                          search_space, best_model_x_coor, best_model_y_coor, description=description+"velocity")

        dev_total, dev_per_person = coor_deviation(
            y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor)

        sample_len = X_x_coor.shape[0]

        if dev_total > dev_total_comp:
            headline = str(0)+"-"+str(train_range_pos-1) + \
                "Predict"+str(current_pred_pos) + "Frame" + \
                "With"+mode
            result_df = add_to_result_df_no_percentage(
                result_df, train_range_pos, current_pred_pos, sample_len, y_hut_x_coor_comp, y_x_coor, y_hut_y_coor_comp, y_y_coor, description=headline)

            if print_traj:
                print_trajectory(y_hut_x_coor_comp, y_x_coor, y_hut_y_coor_comp,
                                 y_y_coor, X_x_coor, X_y_coor, 4, 2, headline=headline)
                if save_path is not None:
                    plt.savefig(save_path+headline+".pdf")

            best_model_x_coor_list.append(best_model_x_coor_comp)
            best_model_y_coor_list.append(best_model_y_coor_comp)
        else:
            headline = str(0)+"-"+str(train_range_pos-1) + \
                "Predict"+str(current_pred_pos) + "Frame" + \
                "WithSVR"
            result_df = add_to_result_df_no_percentage(
                result_df, train_range_pos, current_pred_pos, sample_len, y_hut_x_coor, y_x_coor, y_hut_y_coor, y_y_coor, description=headline)

            if print_traj:
                print_trajectory(y_hut_x_coor, y_x_coor, y_hut_y_coor,
                                 y_y_coor, X_x_coor, X_y_coor, 4, 2, headline=headline)
            if save_path is not None:
                plt.savefig(save_path+headline+".pdf")
            best_model_x_coor_list.append(best_model_x_coor)
            best_model_y_coor_list.append(best_model_y_coor)

    return best_model_x_coor_list, best_model_y_coor_list, result_df

# Print ADE and FDE for the multi_output models


def loss_multi_output(model, X_train, y_train, X_test, y_test):
    y_hut = model.fit(X_train, y_train).predict(X_test)

    y_test_x_coor = y_test[:, :12]
    y_test_y_coor = y_test[:, 12:]

    y_hut_x_coor = y_hut[:, :12]
    y_hut_y_coor = y_hut[:, 12:]

    result_vector = ((((y_test_x_coor - y_hut_x_coor)**2) +
                     ((y_test_y_coor - y_hut_y_coor)**2))**0.5)
    print("ADE ", np.average(result_vector))
    print("FDE ", np.average(result_vector[:, -1]))
