<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->


<!-- PROJECT LOGO -->
<br />

<h3 align="center">Human Trajectory Prediction</h3>

  <p align="center">
    HTP project description
    <br />
    <a href="https://gitlab.eurecom.fr/prostako/malis-final-2021">View Demo</a>
    ·
    <a href="https://gitlab.eurecom.fr/prostako/malis-final-2021/-/issues">Report Bug</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Here's a blank template to get started: To avoid retyping too much info. Do a search and replace with your text editor for the following: `github_username`, `repo_name`, `twitter_handle`, `linkedin_username`, `email`, `email_client`, `project_title`, `project_description`

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [scikit-learn](https://scikit-learn.org)
* [numpy](https://numpy.org)
* [pandas](https://pandas.pydata.org)
* [matplotlib](https://matplotlib.org)
* [pykalman](https://pykalman.github.io)
* [scikit-optimize](scikit-optimize)
* [scipy](https://scipy.org)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- Installation -->
## Installation

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

1. Clone the repo
   ```sh
   git clone git@gitlab.eurecom.fr:prostako/malis-final-2021.git
   ```
2. Install required packages
   ```sh
   pip install pykalman
   pip install scikit-optimize
   ```
3. Run `notebook.ipynb`
   ```sh
   jupyter notebook
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Feature 1
- [ ] Feature 2
- [ ] Feature 3
    - [ ] Nested Feature

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Arina Prostakova - prostako@eurecom.fr

Stefan Weiher - stefan.weiher@eurecom.fr

Prateek Painuly - prateek.painuly@eurecom.fr

Project Link: [https://gitlab.eurecom.fr/prostako/malis-final-2021](https://gitlab.eurecom.fr/prostako/malis-final-2021)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* [Machine Learning and Intelligent Systems course](https://malis-course.github.io)
* [Prof. Maria A. Zuluaga](https://www.eurecom.fr/~zuluaga)

<p align="right">(<a href="#top">back to top</a>)</p>
